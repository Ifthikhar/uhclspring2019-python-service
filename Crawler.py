import requests
from lxml import html
from lxml.etree import ParserError
import json
from time import sleep
import argparse
import unicodecsv as csv
import traceback
from time import gmtime, strftime
import urllib.parse as urlparse
from webargs import fields
from flask_restful import Resource, Api, reqparse
from flask import Flask, abort, request 
import re
import uuid 
from time import sleep
import concurrent.futures
from socket import gethostname


app = Flask(__name__)


@app.route('/foo', methods=['POST']) 
def foo():
    if not request.json:
        abort(400)
    #print ("This username"+request.json['username'])
    return json.dumps(request.json)

@app.route('/crawler',methods=['POST'])
def parse():
	#print("I am starting")
	if not request.json['asin']  and not request.json['condition'] and not request.json['shipping']:
		abort(400, errors= "Data should be in json format. It should contains asin, condition, shipping")
	print(json.dumps(request.json))	 
	asin = request.json['asin']
	print("Asinn...."+asin)
	condition = request.json['condition']
	shipping = request.json['shipping'] # for creating url according to the filter applied
	condition_dict = {'new': 'dp_olp_new_mbc?ie=UTF8&condition=new',
	'used': '&f_used=true',
	'all': '&condition=all',
	'like_new': '&f_usedLikeNew=true',
	'good': '&f_usedGood=true',
	'verygood': '&f_usedVeryGood=true',
	'acceptable': 'f_usedAcceptable=true'
	}
	shipping_dict = {'prime': '&f_primeEligible=true','free':'&f_freeShipping=true','all':''}
	my_asin = asin.split(",")
	url_list = []
	if my_asin:
		for single_asin in my_asin:
			url_list.append('https://www.amazon.com/gp/offer-listing/'+single_asin+'/ref='+condition_dict.get(condition)+shipping_dict.get(shipping))
	return runUrl(url_list,my_asin)

def runUrl(url_list, asin_list):
	jsonResults = {}
	with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
		future_to_url = {executor.submit(parse_offer_details,url_list[i],asin_list[i]): i for i in  range(len(url_list))}
		for future in concurrent.futures.as_completed(future_to_url):
			i = future_to_url[future]
			try:
				data = future.result()
				key = asin_list[i];
				jsonResults[key] = data
			except Exception as exc:
				print('%r generated an exception: %s' % (asin_list[i] , exc))
			else:
				print('%r page is %d bytes' % (asin_list[i], len(data)))
	return json.dumps(jsonResults)
# Retrieve a single page and report the URL and contents
def getBuyBoxSeller(url):
	headers = {
		'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
	}
	seller_winner_buy_box = None
	for retry in range(1):
		try:
			print("::::: Getting the buy box winner seller ID :", url)
			response = requests.get(url, headers=headers, timeout = 5)
			if response.status_code == 403:
				raise ValueError("Captcha found. Retrying")
			response_text = response.text
			parser = html.fromstring(response_text)
			base_url = "https://www.amazon.com/"
			parser.make_links_absolute(base_url)
			XPATH_SELLER_ADD_TO_CART = "//form[@id='addToCart']/input[@name='merchantID']/@value"
			seller_winner_buy_box = parser.xpath(XPATH_SELLER_ADD_TO_CART)
			return seller_winner_buy_box[0] if(seller_winner_buy_box) else None
		except ParserError:
			print("Empty page found")
			seller_winner_buy_box = None
		except:
			print(traceback.format_exc())
			print("Retrying :", url)

def parse_offer_details(url,asin):
	headers = {
		'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
	}
	offer_list = []
	for retry in range(1):
		try:
			print(retry," time. Downloading and processing page :", url)
			response = requests.get(url, headers=headers, timeout = 30)
			if response.status_code == 403:
				raise ValueError("Captcha found. Retrying")
			response_text = response.text
			parser = html.fromstring(response_text)
			base_url = "https://www.amazon.com/"
			parser.make_links_absolute(base_url)
			XPATH_PRODUCT_LISTINGS = "//div[contains(@class, 'a-row a-spacing-mini olpOffer')]"
			listings = parser.xpath(XPATH_PRODUCT_LISTINGS)
			XPATH_PRODUCT_DETAILS = "//a[@id='olpDetailPageLink']/@href"
			detail_url = parser.xpath(XPATH_PRODUCT_DETAILS)
			buybox_winner = getBuyBoxSeller(detail_url[0]) if(detail_url) else None
			buybox_winner = None
			buybox_flag = 0
			updated_rank = 0
			if not listings:
				print("No sellers found")
				offer_list.clear()
			rank = 2 if(buybox_winner) else 1
			for listing in listings: #parsing individual seller
				XPATH_PRODUCT_PRICE = ".//span[contains(@class, 'olpOfferPrice')]//text()"
				XPATH_PRODUCT_PRIME = ".//i/@aria-label"
				XPATH_PRODUCT_SHIPPING = ".//p[contains(@class, 'olpShippingInfo')]//text()"
				XPATH_PRODUCT_CONDITION = ".//span[contains(@class, 'olpCondition')]//text()"
				XPATH_PRODUCT_DELIVERY = ".//div[contains(@class, 'olpDeliveryColumn')]//text()"
				XPATH_PRODUCT_SELLER1 = ".//h3[contains(@class, 'olpSellerName')]//a/text()"
				XPATH_PRODUCT_SELLER2 = ".//h3[contains(@class, 'olpSellerName')]//img//@alt"
				XPATH_PRODUCT_SELLER_RATTING = ".//div[contains(@class, 'olpSellerColumn')]//span[contains(@class, 'a-icon-alt')]//text()"
				XPATH_PRODUCT_SELLER_PERCENTAGE = ".//div[contains(@class, 'olpSellerColumn')]//b/text()"
				XPATH_PRODUCT_SELLER_URL = ".//h3[contains(@class, 'olpSellerName')]//a/@href"
				XPATH_TOTAL_RATING = ".//div[contains(@class, 'a-column a-span2 olpSellerColumn')]//p/text()"
				total_rating = listing.xpath(XPATH_TOTAL_RATING)
				t_rating = (total_rating[2].strip().split("("))[1].split(" total")[0] if total_rating else None
				t_rating = re.sub(',','', t_rating) if(t_rating) else None
				product_price = listing.xpath(XPATH_PRODUCT_PRICE)
				product_price = product_price[0].strip()
				product_price = re.sub('[!@#$]', '', product_price)
				product_prime = listing.xpath(XPATH_PRODUCT_PRIME)
				product_condition = listing.xpath(XPATH_PRODUCT_CONDITION)
				product_shipping = listing.xpath(XPATH_PRODUCT_SHIPPING)
				delivery = listing.xpath(XPATH_PRODUCT_DELIVERY)
				seller1 = listing.xpath(XPATH_PRODUCT_SELLER1)
				seller2 = listing.xpath(XPATH_PRODUCT_SELLER2)
				seller_ratting =listing.xpath(XPATH_PRODUCT_SELLER_RATTING)
				seller_percentage = listing.xpath(XPATH_PRODUCT_SELLER_PERCENTAGE)
				seller_url = listing.xpath(XPATH_PRODUCT_SELLER_URL)
				product_prime = product_prime[0].strip() if product_prime else 0
				product_prime = 1 if(product_prime == "Amazon Prime TM") else 0
				product_condition = ''.join(''.join(product_condition).split()) if product_condition else None
				product_shipping_details = ' '.join(''.join(product_shipping).split()).lstrip("&").rstrip("Details") if product_shipping else None
				is_free_shipping = 1 if(product_shipping_details.find("FREE Shipping")>-1) else 0
				cleaned_delivery = ' '.join(''.join(delivery).split()).replace("Shipping rates and return policy.", "").strip() if delivery else None
				product_seller = ''.join(seller1).strip() if seller1 else ''.join(seller2).strip()
				seller_rating = seller_ratting[0].split()[0].strip() if seller_ratting else None
				seller_percentage = seller_percentage[0].strip() if seller_percentage else None
				seller_percentile = seller_percentage.split("%") if(seller_percentage) else None
				seller_percent = seller_percentile[0] if(seller_percentile) else None
				postive_negative = seller_percentile[1].strip() if(seller_percentile) else None
				seller_sentiment = None if(postive_negative is None) else 1 if (postive_negative == 'positive') else 0
				seller_url = seller_url[0].strip() if seller_url else None
				is_amazon_fulfilled_query = urlparse.urlparse(seller_url,allow_fragments=False) if(seller_url!=None) else 0
				is_amazon_fulfilled = (urlparse.parse_qs(is_amazon_fulfilled_query.query)['isAmazonFulfilled'])[0] if(is_amazon_fulfilled_query!=0) else 0
				seller_id = (urlparse.parse_qs(is_amazon_fulfilled_query.query)['seller'])[0] if(is_amazon_fulfilled_query!=0) else None
				if(seller_id and buybox_winner and (seller_id == buybox_winner)):
					buybox_flag = 1
					updated_rank = rank
					rank = 1
				if (product_seller == 'Amazon.com'):
					is_amazon_fulfilled = 1
				recorded_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
				offer_details = {
					'productPrice': float(product_price),
					'shippingDetails': product_shipping_details,
					'conditionType': product_condition,
					'prime': product_prime,
					'delivery': cleaned_delivery,
					'seller': product_seller,
					'sellerRating': float(seller_rating) if(seller_rating) else None,
					'sellerPercentage': float(seller_percent) if(seller_percent) else None,
					'sellerSentiment': seller_sentiment,
					'sellerUrl':seller_url,
					'asin': asin,
					#'url': url,
					'isFreeShipping': is_free_shipping,
					'isAmazonFullfilled': int(is_amazon_fulfilled),
					#'recordedTime': recorded_time,
					'totalRating': int(t_rating) if (t_rating) else None,
					#'id': str(uuid.uuid4()),
					'rank': rank,
					'sellerId': seller_id ,
					'buyboxWinner': 1 if(rank==1) else 0
					}
				if(rank == 1 and buybox_flag ==1):
					rank =updated_rank	
				rank += 1	
				offer_list.append(offer_details)
		except ParserError:
			print("Empty page found")
			offer_list.clear()
			break
		except:
			print(traceback.format_exc())
			print("retrying :", url)
	return offer_list

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000, debug=True)
#https://www.amazon.com/gp/offer-listing/B076B2HSC6/ref=dp_olp_new_mbc?ie=UTF8&condition=new
#